/*
 * led.c
 *
 *  Created on: 08-Jul-2021
 *      Author: Franky
 */

#include "Includes.h"


void LedInit(void){
	RCC->AHB1ENR |= BV(GPIOD_EN_CLK); // (1 << 3)

	LED_GPIO->MODER |= (BV(2 * Led_Green) | BV(2 * Led_Orange) | BV(2 * Led_Red) | BV (2 * Led_Blue));
	LED_GPIO->MODER &= ~(BV(2 * Led_Green + 1) | BV(2 * Led_Orange + 1) | BV(2 * Led_Red + 1) | BV (2 * Led_Blue + 1));

	LED_GPIO->OTYPER &= ~(BV(Led_Green) | BV(Led_Red) | BV(Led_Orange) | BV(Led_Blue));

	LED_GPIO->OSPEEDR &= ~(BV(2 * Led_Green) | BV(2 * Led_Orange) | BV(2 * Led_Red) | BV (2 * Led_Blue) |
						   BV(2 * Led_Green + 1) | BV(2 * Led_Orange + 1) | BV(2 * Led_Red + 1) | BV(2 * Led_Blue + 1));

	LED_GPIO->PUPDR &= ~(BV(2 * Led_Green) | BV(2 * Led_Orange) | BV(2 * Led_Red) | BV(2 * Led_Blue) |
							   BV(2 * Led_Green + 1) | BV(2 * Led_Orange + 1) | BV(2 * Led_Red + 1) | BV(2 * Led_Blue + 1));

}

void LedON(uint32_t LedPIN){
	LED_GPIO->ODR |= BV(LedPIN);
}

void LedOFF(uint32_t LedPIN){
	LED_GPIO->ODR &= ~BV(LedPIN);
}

void LedBlink(uint32_t LedPIN, uint32_t timeMs){
	LedON(LedPIN);
	DelayMs(timeMs);
	LedOFF(LedPIN);
	DelayMs(timeMs);
}









/*
 * led.h
 *
 *  Created on: 08-Jul-2021
 *      Author: Franky
 */

#ifndef LED_H_
#define LED_H_


//****GPIO Define*****/

#define LED_GPIO				GPIOD
#define GPIOD_EN_CLK			3
#define Led_Green				12
#define Led_Orange				13
#define Led_Red					14
#define Led_Blue				15


void LedInit(void);
void LedON(uint32_t LedPIN);
void LedOFF(uint32_t LedPIN);
void LedBlink(uint32_t LedPIN, uint32_t timeMs);


#endif /* LED_H_ */

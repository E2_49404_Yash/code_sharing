/*
 * Includes.h
 *
 *  Created on: 08-Jul-2021
 *      Author: Franky
 */

#ifndef INCLUDES_H_
#define INCLUDES_H_

#include <stdint.h>
#include <stdio.h>
#include <stm32f407xx.h>
#include <stm32f4xx.h>

#include "led.h"
#include "Switch_Int.h"
#include "Ext_Int.h"


extern volatile uint32_t EXTI0_Flag;


#endif /* INCLUDES_H_ */

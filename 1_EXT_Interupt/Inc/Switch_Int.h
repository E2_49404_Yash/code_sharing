/*
 * Switch_Int.h
 *
 *  Created on: 08-Jul-2021
 *      Author: Franky
 */

#ifndef SWITCH_INT_H_
#define SWITCH_INT_H_

#define Switch_GPIO				GPIOA
#define GPIOA_EN_CLK			0

#define Switch_Pin				0

void Switch_Init(void);
int Switch_Pressed();

#endif /* SWITCH_INT_H_ */
